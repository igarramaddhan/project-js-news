// @flow
import React, {Component} from 'react';
import '../style/index.scss';

import {RouteComponentProps} from 'react-router';
import Scaffold from '../components/Scaffold';
import QueryComponent from '../components/QueryComponent';

const KEY = process.env.NEWS_API_KEY || '';
const queryUrl = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${KEY}`;

type Props = {} & RouteComponentProps;

class Index extends Component<Props> {
  render() {
    return (
      <Scaffold title="News App">
        <QueryComponent url={queryUrl}>
          {({loading, data, error}) => {
            if (loading) return <p style={{marginLeft: 24}}>Loading</p>;
            if (error) return <p>Oops something went wrong</p>;
            return (
              <div className="container">
                <p className="instruction">Top Headlines</p>
                {data.articles.map((article, index) => (
                  <div key={index} className="news-list-item">
                    <img src={article.urlToImage} />
                    <div className="news-list-item-content-container">
                      <a href={article.url} target="__blank">
                        {article.title}
                      </a>

                      <span>by {article.author}</span>

                      <p>Source: {article.source.name}</p>
                    </div>
                  </div>
                ))}
              </div>
            );
          }}
        </QueryComponent>
      </Scaffold>
    );
  }
}

export default Index;
