// @flow
import '../style/scaffold.scss';
import React from 'react';

type Props = {
  children: any,
  title: string,
};

const Scaffold = ({title, children}: Props) => (
  <div className="scaffold-container">
    <div className="header">
      <h3>{title}</h3>
    </div>
    <div className="content">{children}</div>
    <div className="footer">
      <span>
        Powered by{' '}
        <a href="https://newsapi.org" target="__blank">
          News API
        </a>
      </span>
    </div>
  </div>
);

export default Scaffold;
