// @flow
import {useState, useEffect} from 'react';

type Props = {
  children: (props: {loading: boolean, error: any, data: any}) => any,
  url: string,
};

const QueryComponent = ({children, url}: Props) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [state, setData] = useState(null);

  const fetchData = () => {
    fetch(url)
      .then(res => res.json())
      .then(res => {
        if (res.status === 'error') {
          setError(res.message);
        } else {
          setData(res);
        }
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return children({loading, error, data: state});
};

export default QueryComponent;
